package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VendingMachineExceptionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void test() {
		assertEquals(1, 1);
		assertEquals(1, 1);
		//I am not sure how to test exceptions. I might just need to take the loss on this one... or come back to it later -Justin Weagly 
		
		/*
		You should write unit tests for the following classes and functions:
			�	VendingMachineItem
			o	VendingMachineItem Constructor. 
			o	String getName() 
			o	String getPrice() 
			�	VendingMachine
			o	void addItem(VendingMachineItem item, String code) 
			o	VendingMachineItem removeItem(String code) 
			o	void insertMoney(double amount) 
			o	double getBalance() 
			o	boolean makePurchase() 
			o	double returnChange()   
			
			*/

	}

}
